import firebase from 'react-native-firebase';

class Database {
    static getRecipes(cb) {
        firebase.database().ref('/Recipes').on('value', snapshot => {
            cb(snapshot.val())
        })
    }

    static addFavorite(uid, recipe ,cb) {
        firebase.database().ref(`/usersFavs/${uid}`).set({
            [`${recipe.name}`]: recipe
        })
    }

    static getFavorites(uid, cb) {
        const path = '/usersFavs/' + uid;
        firebase.database().ref(path).on('value', snapshot => {
            cb(snapshot.val())
        })
    }
} 

export default Database;