import firebase from 'react-native-firebase';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';

class FirebaseAuth {
  static googleLogin(cb) {
    return GoogleSignin.configure()
      .then(() => {
        GoogleSignin.signIn()
          .then((data) => {
            // create a new firebase credential with the token
            const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
            // login with credential
            return firebase.auth().signInWithCredential(credential)
          })
          .then((currentUser) => {
            cb(currentUser);
          })
      })
      .catch(err => 'configure error: ' + JSON.stringify(err))
  }

  static logout(cb) {
    GoogleSignin.configure()
    .then(() => {
      GoogleSignin.signOut().then(() => {
        cb();
      })
    })
  }
}

export default FirebaseAuth;
