import { StackNavigator } from 'react-navigation';

import HomePage from '../components/HomePage';
import DetailsPage from '../components/DetailsPage';
import LoginPage from '../components/LoginPage';
import AddRecipePage from '../components/AddRecipePage';
import OptionsPage from '../components/OptionsPage';
import FavoritesPage from '../components/FavoritesPage';

const navigator = StackNavigator({
  Home: { screen: HomePage, navigationOptions: { header: null } },
  Login: { screen: LoginPage, navigationOptions: { header: null } },
  Details: { screen: DetailsPage, navigationOptions: { header: null } },
  Add: {screen: AddRecipePage},
  Options: {screen: OptionsPage, navigationOptions: { header: null }},
  Favorites: {screen: FavoritesPage, navigationOptions: { header: null }}
})

export default navigator;

