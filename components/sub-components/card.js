import React, { Component } from 'react';
import {
  Text,
  Image,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';


export default class Card extends Component {

  render() {
    const { recipe } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
        <Image source={{ uri: recipe.pictureUrl }} style={styles.img} />
        <Text style={styles.title}>{recipe.name}</Text>
        <Text style={styles.owner}>By: Ra'fat Assaf</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F1F2F3',
    width: '96%',
    marginLeft: '2%',
    marginBottom: 12,
    height: 250,
    borderRadius: 8
  },
  img: {
    width: '100%',
    height: 180,
    borderRadius: 8
  },
  title: {
    flex: 1,
    alignItems: 'center',
    fontSize: 28,
    width: '100%',
    padding: 6
  },
  owner: {
    flex: 1,
    alignItems: 'center',
    fontSize: 14,
    width: '100%',
    paddingLeft: 10,
    backgroundColor: 'transparent'
  }
})