import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import Swiper from 'react-native-deck-swiper';
import FirebaseDB from '../firebase/database';

export default class DetailsPage extends Component {

  render() {
    const {goBack, navigate} = this.props.navigation;
    const {recipe, uid} = this.props.navigation.state.params;
    return (
      <View style={styles.container}>
        <Swiper
            cards={[
              {
                part: 'cover',
                imgUri: recipe.pictureUrl,
                title: recipe.name
              },
              {
                part: 'ingredients',
                ingredients: recipe.ingredients
              },
              {
                part: 'directions',
                directions: recipe.directions
              },
              {
                part: 'options',
              }]}
            renderCard={(card) => {
              if(card.part === 'cover'){
                return (
                  <View style={styles.card}>
                    <Image style={styles.coverImg} source={{uri: card.imgUri}}/>
                    <Text style={styles.title}>{card.title}</Text>
                    <View style={styles.coverDirections}>
                      <Text style={styles.direction}>Swipe left to go back</Text>
                      <Text style={[styles.direction, {textAlign: 'right', paddingRight: 3}]}>Swipe Right to go next</Text>
                    </View>
                  </View>
                )
              }else if(card.part === 'ingredients'){
                return (
                  <View style={[styles.card, {padding: 20}]}>
                    <Text style={[styles.title, {marginBottom:10}]}>Ingredients</Text>
                    <FlatList
                      keyExtractor={(item, index) => index}
                      data={Object.keys(card.ingredients)}
                      renderItem={(ing) => {
                        return (
                          <View style={[styles.ingredient, {padding: 5}]}>
                            <Text style={styles.ingredientText}>{ing.item}</Text>
                            <Text style={styles.ingredientText}>{card.ingredients[ing.item]}</Text>
                          </View>
                        )
                      }}
                    />
                  </View>
                )
              }else if(card.part === 'directions'){
                return (
                  <View style={[styles.card, {padding: 20}]}>
                    <Text style={[styles.title, {marginBottom:10}]}>Directions</Text>
                    <FlatList
                      keyExtractor={(item, index) => index}
                      data={Object.keys(card.directions).slice(1, 4)}
                      renderItem={(dir) => {
                        return (
                          <View style={[styles.ingredient, {padding: 5}]}>
                            <Text style={styles.ingredientText}>{card.directions[dir.item]}</Text>
                          </View>
                        )
                      }}
                    />
                  </View>
                )
              } else {
                return (
                  <View style={styles.card}>
                    <View style={styles.option}>
                      <Text style={styles.optionText}>Swipe up to add Recipe to favorites</Text>
                    </View>
                    <View style={styles.option}></View>
                    <View style={[styles.option, {justifyContent: 'flex-end'}]}>
                      <Text style={styles.optionText}>Swipe down to go back To homescreen</Text>
                    </View>
                  </View>
                )
              }
              }}
            onSwipedTop={() => {
              FirebaseDB.addFavorite(uid, recipe, () => {
                goBack()
              })
            }}
            onSwipedBottom={(index) => {
              if(index === 3) {
                goBack()
              }
            }}
            onSwipedAll={() => goBack()}
            cardIndex={0}
            backgroundColor={'#FF9900'}
            goBackToPreviousCardOnSwipeLeft
            showSecondCard={false}>
        </Swiper>
        <TouchableOpacity style={styles.backBtn} onPress={() => goBack()}></TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backBtn: {
    width: 60, 
    height: 60,
    borderRadius: 75,
    position: 'absolute',
    top: 30,
    left: 3,
    backgroundColor: '#3E4377'
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'flex-start',
    backgroundColor: '#F1F2F3',
  },
  title: {
    fontSize: 50,
    backgroundColor: 'transparent',
    padding: '2%'
  },
  coverImg: {
    width: '96%',
    margin: '2%',
    height: 300
  },
  coverDirections: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
    alignItems: 'flex-end'
  },
  direction: {
    flex: 1,
    fontSize: 12,
  },
  ingredient: {
    flexDirection: 'row'
  },
  ingredientText: {
    fontSize: 18,
    flex: 1,
    borderBottomWidth: 2,
    borderBottomColor: '#3E4377',
    padding: 2,
  },
  option: {
    flex: 1,
    alignItems: 'center',
    padding: 20
  },
  optionText: {
    fontSize: 15,
    textAlign: 'center',
  }
  
})