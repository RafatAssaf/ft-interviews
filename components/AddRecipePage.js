import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TextInput,
} from 'react-native';

export default class AddRecipePage extends Component {

  constructor () {
    super();
    this.state = {
      recipeName: '',
    }
  }

  render() {
    const {goBack} = this.props.navigation
    return (
      <View style={styles.container}>
        <Text>{JSON.stringify(this.state)}</Text>
        <Text>Recipe Name</Text>
        <TextInput
          style={styles.name}
          placeholder="My Amazing Recipe Name"
          underlineColorAndroid="transparent"
          onChangeText={(text) => this.setState({recipeName: text})}
        />
        <Text>Recipe Name</Text>
        <TextInput
          style={styles.name}
          placeholder="My Amazing Recipe Name"
          underlineColorAndroid="transparent"
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF9F68'
  },
  name: {
    width: '80%',
    height: 60,
    backgroundColor: '#F1F2F3',
    borderRadius: 45,
    padding: 20
  }
})