import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  AsyncStorage,
  FlatList,
  TouchableOpacity,
  StatusBar
} from 'react-native';

import FirebaseDB from '../firebase/database';
import Card from '../components/sub-components/card';


export default class FavoritesPage extends Component {

  constructor() {
    super();
    this.state = {
      favorites: [],
      uid: ''
    }

    this.getFavorites()
  }

  componentWillMount() {
    this.setState({uid: this.props.navigation.state.params.uid})
  }

  getFavorites = () => {
    // const {uid} = this.props.navigation.state.params;
    FirebaseDB.getFavorites(this.state.uid ,(favorites) => {

      //A LOT of mess here!!!! 
      this.setState({favorites: [
        favorites[Object.keys(favorites)[0]][
          Object.keys(
            favorites[Object.keys(favorites)[0]]
          )[0]
        ]
      ]})
    })
  }

  renderRecipe = (recipe) => {
    return (
      // <Text style={styles.testText}>{JSON.stringify(recipe.item.name)}</Text>
      <Card 
        recipe={recipe.item} 
        onPress={() => this.navigateToDetailsPage(recipe.item)}>
      </Card>
    )
  }

  render() {

    // alert(JSON.stringify(this.state.favorites))
    const {goBack} = this.props.navigation;
    return(
      <View style={styles.container}>
      <StatusBar 
        backgroundColor='#FD367E'
      /> 
      <View style={styles.topBar}>
        <TouchableOpacity style={styles.optionsBtn} onPress={() => {goBack()}}>
          <Text style={styles.optionsBtnText}>Home</Text>
        </TouchableOpacity>
        <Text style={styles.textLogo}>Favorites !</Text>        
      </View>
      <FlatList
        onScroll={() => {}}
        data={this.state.favorites}
        renderItem={this.renderRecipe}
        style={styles.cardsList}
        keyExtractor={(item, index) => index}
      />
    </View> 
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3E4377'
  },
  cardsList: {
    marginTop: 10,
    width: '100%',
    zIndex: 2,
  },
  textLogo: {
    flex: 2,
    fontFamily: 'cursive',
    color: '#FD367E',
    fontSize: 30,
    fontWeight: '900',
    margin: 8,
    marginBottom: 2,
  },
  topBar: {
    flexDirection: 'row'
  },
  optionsBtn: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10
  },
  optionsBtnText: {
    fontWeight: '900',
    fontSize: 17,
    textAlign: 'left',
    paddingTop: 10,
    fontFamily: 'sans',
    color: '#F1F2F3'
  },
  testText: {
    color: 'white',
    fontSize: 10
  }
})