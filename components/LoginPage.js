import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  AsyncStorage
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { GoogleSigninButton } from 'react-native-google-signin';

import FirebaseAuth from '../firebase/auth';
import { actionCreators } from '../reducers/authReducer';

export default class LoginPage extends Component {

  constructor() {
    super();
    AsyncStorage.getItem('USER_DATA').then((user) => {
      if(user){
        this.handleUserLogin(JSON.parse(user))
      }
    });
  }

  
  handleUserLogin = (user) => {
    let action = actionCreators.authenticate(user);
    this.props.navigation.dispatch(action);
    AsyncStorage.setItem('USER_DATA', JSON.stringify(user))
    .then(() => {
      this.props.navigation.dispatch(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({routeName: 'Home', params: {user: user}}),
          ]
      }))
    })
  };

  render() {

    return (
      <View style={styles.square}>
        <Text style={styles.welcome}>WELCOME TO</Text>
        <Text style={styles.appName}>FriendCook !</Text>
        <GoogleSigninButton
          style={{width: 250, height: 48}}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          onPress={() => {FirebaseAuth.googleLogin(this.handleUserLogin)}}        
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  welcome: {
    fontSize: 20,
    fontFamily: 'roboto',
    margin: 0,
    color: '#3E4377',
    fontWeight: '900',
  },
  appName: {
    fontSize: 70,
    marginBottom: 85,
    fontWeight: '900',
    color: '#FF9900',
    fontFamily: 'cursive'
  },
  square: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F1F2F3'
  }
});