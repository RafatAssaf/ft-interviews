import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  AsyncStorage
} from 'react-native';
import FirebaseAuth from '../firebase/auth';
import { actionCreators } from '../reducers/authReducer';
import { NavigationActions } from 'react-navigation';


export default class OptionsPage extends Component {

  logout() {
    AsyncStorage.removeItem('USER_DATA')
    .then(() => {
      let action = actionCreators.logout();
      this.props.navigation.dispatch(action);
      this.props.navigation.dispatch(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({routeName: 'Login'}),
          ]
      }))
    })
  }

  navigateToFavoritesPage = () => {
    const {user} = this.props.navigation.state.params;
    this.props.navigation.dispatch(
      NavigationActions.navigate({routeName: 'Favorites', params: {
        uid: user.uid.match(/\d+$/)[0]
      } })
    )
  }

  render() {
    const {photoURL, displayName} = this.props.navigation.state.params.user;
    const {goBack} = this.props.navigation;
    return (
      <View style={styles.container}>
        <Image source={{uri: photoURL}} style={styles.userImg}/>
        <Text style={styles.username}>{displayName}</Text>
        <TouchableOpacity style={styles.optionBtn} onPress={() => this.navigateToFavoritesPage()}>
          <Text style={styles.optionText}>Favorites</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.optionBtn} onPress={() => {this.logout()}}>
          <Text style={styles.optionText}>Logout</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.backBtn} onPress={() => goBack()}></TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backBtn: {
    width: 60, 
    height: 60,
    borderRadius: 75,
    position: 'absolute',
    top: 30,
    left: 20,
    backgroundColor: '#3E4377'
  },
  userImg: {
    width: 150, 
    height: 150,
    borderRadius: 75,
    marginBottom: 20,
  },
  username: {
    fontSize: 25,
    marginBottom: 30
  },
  optionBtn: {
    width: '60%',
    height: 50,
    backgroundColor: '#3E4377',
    borderRadius: 25,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  optionText: {
    color: 'white',
    fontSize: 20
  }
})