import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  AsyncStorage,
  FlatList,
  TouchableOpacity,
  StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import FirebaseDB from '../firebase/database';
import Card from './sub-components/card';

class HomePage extends Component {

  constructor() {
    super();
    this.state = {
      recipes: [],
      user: {}
    }

    this.getRecipes()
  }

  componentWillMount() {
    this.setState({user: this.props.navigation.state.params.user.providerData[0]})
  }

  getRecipes = () => {
    FirebaseDB.getRecipes((recipes) => {
      this.setState({recipes: recipes})
    })
  }

  navigateToDetailsPage = (recipe) => {
    this.props.navigation.dispatch(
      NavigationActions.navigate({routeName: 'Details', params: {
        recipe: recipe,
        uid: this.state.user.uid.match(/\d+$/)[0]
      }})
    )
  }

  navigateToOptionsPage = () => {
    this.props.navigation.dispatch(
      NavigationActions.navigate({routeName: 'Options', params: {user: this.state.user}})
    )
  }

  renderRecipe = (recipe) => {
    return (
      <Card 
        recipe={recipe.item} 
        onPress={() => this.navigateToDetailsPage(recipe.item)}>
      </Card>
    )
  }

  render() {
    // alert(JSON.stringify(this.state.user))
    const username = this.state.user.displayName;
    return (
      <View style={styles.container}>
        <StatusBar 
          backgroundColor='#FF9900'
        /> 
        <View style={styles.topBar}>
          <TouchableOpacity style={styles.optionsBtn} onPress={() => {this.navigateToOptionsPage()}}>
            <Text style={styles.optionsBtnText}>{username}</Text>
          </TouchableOpacity>
          <Text style={styles.textLogo}>FriendCook !</Text>        
        </View>
        <FlatList
          onScroll={() => {}}
          data={this.state.recipes}
          renderItem={this.renderRecipe}
          style={styles.cardsList}
          keyExtractor={(item, index) => index}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3E4377'
  },
  cardsList: {
    marginTop: 10,
    width: '100%',
    zIndex: 2
  },
  textLogo: {
    flex: 2.2,
    fontFamily: 'cursive',
    color: '#FF9900',
    fontSize: 30,
    fontWeight: '900',
    margin: 8,
    marginBottom: 2,
  },
  topBar: {
    flexDirection: 'row'
  },
  optionsBtn: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10
  },
  optionsBtnText: {
    fontWeight: '900',
    fontSize: 17,
    textAlign: 'left',
    paddingTop: 10,
    fontFamily: 'sans',
    color: '#F1F2F3'
  }
})

const mapStateToProps = state => ({
  isAuthenticated: state.AuthReducer.isAuthenticated
})

export default connect(mapStateToProps)(HomePage)