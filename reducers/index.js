import { combineReducers } from 'redux';
import NavigationReducer from './navigationReducer';
import RecipeReducer from './recipeReducer';
import AuthReducer from './authReducer';

const AppReducer = combineReducers({
    NavigationReducer,
    RecipeReducer,
    AuthReducer
})

export default AppReducer;