
const types = {
  USER_LOGIN: 'USER_LOGIN',
  USER_LOGOUT: 'USER_LOGOUT'
}

export const actionCreators = {
  authenticate: (user) => ({ type: types.USER_LOGIN, payload: user }),
  logout: () => ({ type: types.USER_LOGOUT })
}

const initialState = {
  isAuthenticated: false,
  user: undefined
}

const authReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.USER_LOGIN: {
      return {
        ...state,
        isAuthenticated: true,
        user: payload
      }
    }
    case types.USER_LOGOUT: {
      return {
        ...state,
        isAuthenticated: false,
        user: undefined
      }
    }
    default: {
      return state
    }
  }
}

export default authReducer;