
const types = {
  ADD_RECIPE: 'ADD_RECIPE'
}

const actionCreators = {
  addRecipe: (recipe) => ({type: types.ADD_RECIPE, payload: recipe})
}

const initialState = {
  recipes: ['recipe1', 'recipe2', 'recipe3']
}

const recipeReducer = (state = initialState, action) => {
  const { type, payload } = action;
  const { recipes } = state;
  switch (type) {
    case types.ADD_RECIPE: {
      return {
        ...state,
        recipes: [payload, ...recipes]
      }
    }
    default: {
      return state
    }
  }
}

export default recipeReducer;