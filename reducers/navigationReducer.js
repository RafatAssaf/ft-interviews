import NavigationStack from '../navigation/navigationStack';

const initialState = NavigationStack.router.getStateForAction(
  NavigationStack.router.getActionForPathAndParams('Login')
)

const navigationReducer = (state = initialState, action) => {
  const newState = NavigationStack.router.getStateForAction(action, state);
  return newState || state;
}

export default navigationReducer;