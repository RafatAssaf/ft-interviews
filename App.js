import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Provider } from 'react-redux';
import store from './Store';
import NavigationStack from './navigation';


export default class App extends Component{ 
  render() {
    return (
      <Provider store={store}>
        <NavigationStack />
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  
});

// import { StackNavigator } from 'react-navigation';

// import HomePage from './components/HomePage';
// import DetailsPage from './components/DetailsPage';
// import LoginPage from './components/LoginPage';
// import AddRecipePage from './components/addRecipePage';

// const RootNavigator = StackNavigator({
//   Home: {screen: HomePage},
//   Login: {screen: LoginPage, navigationOptions: { header: null }},
//   Details: {screen: DetailsPage},
//   Add: {screen: AddRecipePage}
// },{
//   initialRouteName: 'Login',
// })

// export default class App extends Component {
//   render() {
//     return (
//       <RootNavigator />
//     );
//   }
// }